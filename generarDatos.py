import random

# { } Conjunto de valores que no se repiten
datos = set()
f = open("datos.txt", "w")
while True:
  if len(datos) == 50000:
    break
  v = random.randint(0, 100000)
  datos.add(v)

arr = list(datos)
random.shuffle(arr)

for i in arr:
  f.write(str(i)+"\n")

f.close()




"""
 f = open("demofile3.txt", "w")
f.write("Woops! I have deleted the content!")
f.close()

#open and read the file after the appending:
f = open("demofile3.txt", "r")
print(f.read()) 
"""