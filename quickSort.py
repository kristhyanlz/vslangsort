import time
import math

def promedio(arr):
	return sum(arr)/len(arr)

def desvStd(arr):
	media = promedio(arr)
	suma = 0
	for i in arr:
		actual = pow(i - media, 2)
		suma += actual

	return math.sqrt(suma/len(arr))

def partition(arr, low, high):
    pivot = arr[high]
    i = low - 1
    for j in range(low, high):
        if arr[j] < pivot:
            i += 1
            arr[i], arr[j] = arr[j], arr[i]

    arr[i + 1], arr[high] = arr[high], arr[i + 1]
    return i + 1

def quicksort2(arr, low, high):
    if (low < high):
        pi = partition(arr, low, high)

        quicksort2(arr, low, pi - 1)
        quicksort2(arr, pi + 1, high)


def quicksort(arr):
    quicksort2(arr, 0, len(arr) - 1)

if __name__ == "__main__":
	lstTam = [100, 1000, 2000, 3000, 4000, 5000, 6000, 7000,
						8000, 9000, 10000, 20000, 30000, 40000, 50000]

	nomFile = "datos.txt"
	nums = []
	
	with open(nomFile, "r") as datos:
		for linea in datos:
			nums.append( int(linea) )

	print("PYTHON\n")

	for tam in lstTam:
		tiempos = []
		for _ in range(5):
			subArray = nums[:tam]

			t1 = time.time()
			quicksort(subArray)
			t2 = time.time()

			tiempos.append( (t2 - t1) * 1000 )

		print(f"Tamaño: {tam} Promedio: {promedio(tiempos)}",
					f"devStd: {desvStd(tiempos)}")