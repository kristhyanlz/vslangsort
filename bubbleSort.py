import time
import math

def promedio(arr):
	return sum(arr)/len(arr)

def desvStd(arr):
	media = promedio(arr)
	suma = 0
	for i in arr:
		actual = pow(i - media, 2)
		suma += actual

	return math.sqrt(suma/len(arr))
 
def bubbleSort(arr):
    n = len(arr)
 
    # Traverse through all array elements
    for i in range(n):
 
        # Last i elements are already in place
        for j in range(0, n-i-1):
 
            # traverse the array from 0 to n-i-1
            # Swap if the element found is greater
            # than the next element
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]

if __name__ == "__main__":
	lstTam = [100, 1000, 2000, 3000, 4000, 5000, 6000, 7000,
						8000, 9000, 10000, 20000, 30000, 40000, 50000]

	nomFile = "datos.txt"
	nums = []
	
	with open(nomFile, "r") as datos:
		for linea in datos:
			nums.append( int(linea) )

	print("PYTHON\n")

	for tam in lstTam:
		tiempos = []
		for _ in range(5):
			subArray = nums[:tam]

			t1 = time.time()
			bubbleSort(subArray)
			t2 = time.time()

			tiempos.append( (t2 - t1) * 1000 )

		print(f"Tamaño: {tam} Promedio: {promedio(tiempos)}",
					f"devStd: {desvStd(tiempos)}")
