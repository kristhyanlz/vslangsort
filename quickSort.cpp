#include <iostream>
#include <fstream>
#include <chrono>
#include <cmath>

#define TAM 50000


using namespace std;
using namespace std::chrono;

void copyArr(int * origen, int * dest, int tam){
	for(register int i = 0; i < tam; ++i){
		dest[i] = origen[i];
	}
}

template <class numero>
double promedio(numero * arr, int tam){
	double r = 0;
	for(register int i = 0; i < tam; ++i){
		r += arr[i];
	}
	return r/tam;
}

template <class numero>
double desvStd(numero *arr, int tam){
	double media = promedio(arr, tam);
	double sum = 0;
	for(register int i = 0; i < tam; ++i){
		double actual = arr[i] - media;
		actual = actual * actual;
		sum += actual;
	}
	return sqrt(sum/tam);
}

void swap(int* a, int* b)
{
    int t = *a;
    *a = *b;
    *b = t;
}

int partition (int arr[], int low, int high)
{
    int pivot = arr[high]; 
    int i = (low - 1); 

    for (int j = low; j < high; j++){
        if (arr[j] < pivot){
            i++; 
            swap(&arr[i], &arr[j]);
        }
    }
    
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

void quicksort2(int arr[], int low, int high){
    if (low < high){
        int pi = partition(arr, low, high);

        quicksort2(arr, low, pi - 1);
        quicksort2(arr, pi + 1, high);
    }
}

void quicksort(int arr [], int n){
    quicksort2(arr, 0, n - 1);
}

int main(int argc, char * argv []){
	int lstTam[] = {100, 1000, 2000, 3000, 4000, 5000, 6000, 7000,
							8000, 9000, 10000, 20000, 30000, 40000, 50000};
	
	string nomFile = "datos.txt";
	ifstream datos(nomFile.c_str());
	int nums[TAM];

	string linea;
	for (register int i = 0; i < TAM; ++i){
		getline(datos, linea);
		nums[i] = stoi(linea);
	}	

	for (auto tam: lstTam){
		double tiempos[5];
		for(int i = 0; i < 5; ++i){
			int subArray[tam];
			copyArr(nums, subArray, tam);

			auto t1 = high_resolution_clock::now();
			quicksort(subArray, tam);
			auto t2 = high_resolution_clock::now();
			
			tiempos[i] = (duration<double, milli>(t2 - t1)).count();
		}
		cout << "Tamaño: " << tam 
				<< " Promedio: " << promedio<double>(tiempos, 5)
				<< " desvStd: " << desvStd<double>(tiempos, 5) << endl;
	} 
}
