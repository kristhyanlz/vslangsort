package main

import (
	"fmt"
	"math"
	"bufio"
	"os"
	"strconv"
	"time"
)

func promedio(arr []float64) float64{
	var r float64 = 0
	for _, num := range arr{
		r += num
	}
	return r/float64(len(arr))
}

func desvStd(arr []float64) float64{
	media := promedio(arr)
	var suma float64 = 0

	for _, num := range arr{
		var actual float64= math.Pow(num - media, 2)
		suma += actual
	}
	return math.Sqrt(suma/float64(len(arr)))
}

func partition(arr []int, low int, high int) int{
  pivot := arr[high]
  i := low - 1

  for j := low; j < high; j++{
    if (arr[j] < pivot){
      i++
      arr[i], arr[j] = arr[j], arr[i]
    }
  }
  arr[i + 1], arr[high] = arr[high], arr[i + 1]
  return i + 1
}

func quicksort2(arr []int, low int, high int){
  if (low < high){
    pi := partition(arr, low, high)

    quicksort2(arr, low, pi - 1)
    quicksort2(arr, pi + 1, high)
  }
}

func quicksort(arr []int){
  quicksort2(arr, 0, len(arr) - 1)
}


func main(){
	var lstTam = []int {100, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 20000, 30000, 40000, 50000}

	nums := make([]int, 0)

	fmt.Println("GOLANG")

	readFile, _ := os.Open("datos.txt")

	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)
	i := 0
	for fileScanner.Scan(){
		nn, _ := strconv.Atoi(fileScanner.Text())
		nums = append(nums, nn)
	}

	for _, tam := range lstTam{
		//var tiempos [5]float64
		tiempos := make([]float64, 0)
		for i = 0; i < 5; i++{
			//var subArray [tam] int
			//subArray := make([]int, 0)
			subArray := nums[0:tam]
			//copy(nums[0:tam], subArray[:])

			//fmt.Println("Tam Array: ", len(subArray))

			t1 := time.Now()
			quicksort(subArray)
			tt := time.Since(t1)
			//fmt.Println("Tiempo!!", tt)
			tiempos = append(tiempos, float64(tt)/1000000 )
		}
		fmt.Printf("Tamaño: %d Promedio: %f desvStd: %f\n", tam,
								promedio(tiempos[:]),
								desvStd(tiempos[:]))
	}
}
